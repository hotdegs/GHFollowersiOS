//
//  Constants.swift
//  GHFollowers
//
//  Created by Steven Volocyk on 2/12/20.
//  Copyright © 2020 Steven Volocyk. All rights reserved.
//

import Foundation

enum SFSymbols {
  static let location = "mappin.and.ellipse"
}
